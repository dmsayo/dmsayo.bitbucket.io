var plot_8py =
[
    [ "color", "plot_8py.html#a9b064c75bf32f4c3346c4256e2426b48", null ],
    [ "label", "plot_8py.html#ab833ccc4e4fb7e634ddb35e4eb25c59d", null ],
    [ "line", "plot_8py.html#a72f95127a17220dcf0adf38386b72ae4", null ],
    [ "P_VAL", "plot_8py.html#a848589100e237e9971e6603d3ea73b11", null ],
    [ "pos", "plot_8py.html#ae4441c036c067a3291c59db6f5106261", null ],
    [ "ser", "plot_8py.html#a88fb5d9cd325b2a44c69f443fad97b90", null ],
    [ "step", "plot_8py.html#a3009483013aff215b2d1e96558fe3e1c", null ],
    [ "t", "plot_8py.html#a559656b08963695ed27fc5545733c6c7", null ],
    [ "vals", "plot_8py.html#a5092e5230017f430021f345f123b9abb", null ],
    [ "xmax", "plot_8py.html#a685a477f2faa3861af7ac0df03ce705f", null ],
    [ "xmin", "plot_8py.html#aeec0b24fcca8eac78b0e5c25ab03d858", null ],
    [ "ymax", "plot_8py.html#a1a2b9beb177b76125bf1280dc9a42d72", null ],
    [ "ymin", "plot_8py.html#af18ac286c7708413f936ed6e7a3f6a99", null ]
];