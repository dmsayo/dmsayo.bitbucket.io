var nunchuk_8py =
[
    [ "Nunchuk", "classnunchuk_1_1Nunchuk.html", "classnunchuk_1_1Nunchuk" ],
    [ "CALIB_ADDR", "nunchuk_8py.html#a01ffb1805211ce062b5c9467fbcba54c", null ],
    [ "chuck", "nunchuk_8py.html#af39d2e6f8e7a724f358f599423c8bd9b", null ],
    [ "DATA_ADDR", "nunchuk_8py.html#a603842f05237265932f5f78130194240", null ],
    [ "end", "nunchuk_8py.html#ab64b43f3e403f7012ee90145895be561", null ],
    [ "JOY_X_SW_CALIB", "nunchuk_8py.html#adc821c6a7c0275a80dd2748b75e7827f", null ],
    [ "JOY_Y_SW_CALIB", "nunchuk_8py.html#a4e5ca2fea11fcf67277c792f873e3fd6", null ],
    [ "NUNCHUK_DISABLE_ENCRYPTION", "nunchuk_8py.html#ac181dfa4ab3c5e601c485401aec0eef5", null ]
];