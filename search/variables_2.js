var searchData=
[
  ['cal_5fstat_306',['cal_stat',['../namespaceimu.html#adc85732f35dac32a3d8a6ebd03b5fa94',1,'imu']]],
  ['calib_307',['calib',['../classnunchuk_1_1Nunchuk.html#adf0637678e1ba22d6915bc7c980da3f7',1,'nunchuk::Nunchuk']]],
  ['calib_5faddr_308',['CALIB_ADDR',['../namespacenunchuk.html#a01ffb1805211ce062b5c9467fbcba54c',1,'nunchuk']]],
  ['calib_5fstat_5faddr_309',['CALIB_STAT_ADDR',['../namespaceimu.html#a1d9b8cec2a0cab19a2165e2be4a80958',1,'imu']]],
  ['chuck_310',['chuck',['../main_8py.html#a998dc8c3d825c751af708efec788da2b',1,'main.chuck()'],['../namespacenunchuk.html#af39d2e6f8e7a724f358f599423c8bd9b',1,'nunchuk.chuck()']]],
  ['cnt_311',['cnt',['../classencoder_1_1Encoder.html#a350e6ac9a38a538c8cdcc61d82dea3ee',1,'encoder::Encoder']]],
  ['ctrl_312',['ctrl',['../namespacecontrol.html#a177aa39b37399412f2a721c406c08842',1,'control']]],
  ['cur_5fangle_313',['cur_angle',['../main_8py.html#a99dc1931e0b962c66656eaf49705a4aa',1,'main']]],
  ['cur_5fduty_314',['cur_duty',['../namespacecontrol.html#a20f17cc4ebdaa1d88e6dd05b9cbf6fc5',1,'control']]],
  ['cur_5fpos_315',['cur_pos',['../namespacecontrol.html#a2dc2244b7ba7c428ae5c2d53c27f4428',1,'control']]],
  ['curr_5fduty_316',['curr_duty',['../motor__driver_8py.html#a665436740c97db5c72d0e2e32d0fa78a',1,'motor_driver']]],
  ['curs_5foff_317',['CURS_OFF',['../namespacelcd.html#a7c07efb7c21dc32b3f343d417b1704a0',1,'lcd']]],
  ['curs_5fon_318',['CURS_ON',['../namespacelcd.html#a2b75a31cd202c8128ed82a79684cfe36',1,'lcd']]]
];
