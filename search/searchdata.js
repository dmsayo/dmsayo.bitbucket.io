var indexSectionsWithContent =
{
  0: "_abcdefghijklmnoprstuvwxy",
  1: "ceilmn",
  2: "ceilmn",
  3: "cefilmnp",
  4: "_bcdefghijlmnprsuw",
  5: "abcdefgijklmnoprstvxy",
  6: "fip"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

