var searchData=
[
  ['get_5fang_5fvelo_80',['get_ang_velo',['../classimu_1_1IMU.html#a98e5d7abd98b98c3fa4e3ed35fc97e78',1,'imu::IMU']]],
  ['get_5fcalibration_81',['get_calibration',['../classnunchuk_1_1Nunchuk.html#a1e1b7288ece2adf40e0b24889527b0f3',1,'nunchuk::Nunchuk']]],
  ['get_5fcalibration_5fdata_82',['get_calibration_data',['../classimu_1_1IMU.html#a62205bf8310e73d52aef722e5ba9f7e8',1,'imu::IMU']]],
  ['get_5fcalibration_5fstatus_83',['get_calibration_status',['../classimu_1_1IMU.html#a828ee3d84a4c68e0fba9c394d2aca952',1,'imu::IMU']]],
  ['get_5fdelta_84',['get_delta',['../classencoder_1_1Encoder.html#a2f451b6cb3e85e03d45e0ac097e29a29',1,'encoder::Encoder']]],
  ['get_5forientation_85',['get_orientation',['../classimu_1_1IMU.html#a63b091dace5f56106afa89801fcafce3',1,'imu::IMU']]],
  ['get_5fposition_86',['get_position',['../classencoder_1_1Encoder.html#abc44b0bb3d2ee93571f00d6bab5e7c53',1,'encoder::Encoder']]],
  ['gpio_87',['gpio',['../classlcd_1_1LCD.html#a101a55273e5cd8dab2d28c969c5bc3a1',1,'lcd::LCD']]],
  ['gyr_5fdata_5fx_5faddr_88',['GYR_DATA_X_ADDR',['../namespaceimu.html#a73dba8d19c7701f6eb69d0ff0c9f05ba',1,'imu']]],
  ['gyr_5fdata_5fy_5faddr_89',['GYR_DATA_Y_ADDR',['../namespaceimu.html#ae81960d9295f4554f59351b3065b75c5',1,'imu']]],
  ['gyr_5fdata_5fz_5faddr_90',['GYR_DATA_Z_ADDR',['../namespaceimu.html#ae7a7a6ddcadb59fea926b99c55457eb3',1,'imu']]],
  ['gyr_5foffset_5faddr_91',['GYR_OFFSET_ADDR',['../namespaceimu.html#a73ac3acc7727eccd48cb1889e4804b02',1,'imu']]]
];
