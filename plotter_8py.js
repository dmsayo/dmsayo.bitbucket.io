var plotter_8py =
[
    [ "color", "plotter_8py.html#a05b950e24ee2d01159e14ebeec669200", null ],
    [ "label", "plotter_8py.html#ad86fcffa76050cfefcf5c40741ce3337", null ],
    [ "line", "plotter_8py.html#a6655086b7254b332a52cfefff98d39f4", null ],
    [ "P_VAL", "plotter_8py.html#a565c6da6f8cb5d0ebaf0c19e5608947d", null ],
    [ "pos", "plotter_8py.html#a5784c80d4e3fa42fe01e3c8ba8f51e90", null ],
    [ "ser", "plotter_8py.html#abc5a599bbfe886c92c243c3abb9b0726", null ],
    [ "step", "plotter_8py.html#affe1c9b7d1aaba9ccd82bfa71f147c30", null ],
    [ "t", "plotter_8py.html#ab5f9cd1607d254fece38a050904b9176", null ],
    [ "vals", "plotter_8py.html#a90ce2bb5647154a2230099e88ab690ca", null ],
    [ "xmax", "plotter_8py.html#a9ad65d46fe012db0a8cf83ad3f97d888", null ],
    [ "xmin", "plotter_8py.html#aa1d3262e68b70f1eae846db004543641", null ],
    [ "ymax", "plotter_8py.html#a67856e656f072633cfa30d997dc79a69", null ],
    [ "ymin", "plotter_8py.html#afeb69e1ae38b3689bfbc6411ff5aef67", null ]
];