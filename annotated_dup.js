var annotated_dup =
[
    [ "control", "namespacecontrol.html", "namespacecontrol" ],
    [ "encoder", "namespaceencoder.html", "namespaceencoder" ],
    [ "imu", "namespaceimu.html", "namespaceimu" ],
    [ "lcd", "namespacelcd.html", "namespacelcd" ],
    [ "motor_driver", null, [
      [ "Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "nunchuk", "namespacenunchuk.html", "namespacenunchuk" ]
];